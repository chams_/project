import './Advert.css';

const adverts = [
    {
        id: 1,
        content: 'Mac OS X',
        date: '1623487827',
        category: 'pcs'
    },
    {
        id: 2,
        content: 'Galaxy S10',
        date: '1623487827',
        category: 'mobiles'
    },
    {
        id: 3,
        content: 'Peugeot 407',
        date: '1623487827',
        category: 'cars'
    }
]
const Adverts = () => {
    const addAdvert = (event) => {
        event.preventDefault()
        console.log('button clicked', event.target)
    }
    return (
        <div>
            <h2>Adverts</h2>
            <ul>
                {adverts.map(ad =>
                    <li key={ad.id}>
                        {ad.content}
                    </li>
                )}
            </ul>
            <p className="adv-content">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
            </p>
            <form onSubmit={addAdvert} >
                <input />
                <button type="submit">Save</button>
            </form>
        </div>
    )
}

export default Adverts;