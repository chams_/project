import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./components/home/Home";
import Adverts from "./components/adverts/Adverts";
import Users from "./components/users/Users";
import Categories from "./components/categories/Categories";
import './App.css';

const App = () => {

  const [adverts, setAdverts] = useState([])

  return (
    <Router>
      <div>
        <Link className="App-link" to="/">Home</Link>
        <Link className="App-link" to="/adverts">Adverts</Link>
        <Link className="App-link" to="/categories">Categories</Link>
        <Link className="App-link" to="/users">Users</Link>
      </div>

      <Switch>
        <Route path="/categories">
          <Categories />
        </Route>
        <Route path="/users">
          <Users />
        </Route>
        <Route path="/adverts">
          <Adverts adverts={adverts} />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
